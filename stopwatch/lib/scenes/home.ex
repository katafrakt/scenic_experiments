defmodule Stopwatch.Scene.Home do
  use Scenic.Scene

  alias Scenic.Graph
  alias Scenic.ViewPort

  import Scenic.Primitives

  @frame_ms 7

  def init(_, opts) do
    viewport = opts[:viewport]
    {:ok, %ViewPort.Status{size: {vp_width, vp_height}}} = ViewPort.info(viewport)

    graph = Graph.build(font: :roboto, font_size: 52)

    {:ok, timer} = :timer.send_interval(@frame_ms, :frame)

    state = %{
      timer: timer,
      graph: graph,
      start_time: Time.utc_now,
      vp_width: vp_width,
      vp_height: vp_height,
      running: false,
      last_lap_time: nil,
      previous_lap_time: nil
    }

    draw(state, [0, 0, 0])
    {:ok, state}
  end

  defp draw(state, current_time) do
    center_coords = {state.vp_width / 2, state.vp_height / 2}

    state.graph
    |> text(format_time(current_time), translate: center_coords, text_align: :center)
    |> text(last_lap_text(state), translate: {state.vp_width / 2, (state.vp_height / 2) + 40}, text_align: :center, font_size: 30)
    |> text("(space: start/lap, esc: stop)", translate: {state.vp_width / 2, state.vp_height - 20}, text_align: :center, font_size: 20)
    |> push_graph()
  end

  defp last_lap_text(%{previous_lap_time: nil} = state) do
    ""
  end

  defp last_lap_text(state) do
    text = time_diff_to_parts(state.last_lap_time, state.previous_lap_time) |> format_time()
    "Last lap: #{text}"
  end

  defp format_time(parts) do
    parts
    |> Enum.map(&(String.pad_leading(to_string(&1), 2, "0")))
    |> Enum.join(":")
  end

  defp time_diff_to_parts(start_time, end_time) do
    diff = Time.diff(start_time, end_time, :millisecond) |> div(10)
    [
      div(diff, 6000),
      rem(div(diff, 100), 60),
      rem(diff, 100)
    ]
  end

  def handle_info(:frame, %{running: false} = state) do
    {:noreply, state}
  end

  def handle_info(:frame, %{running: true} = state) do
    time = time_diff_to_parts(Time.utc_now, state.start_time)
    draw(state, time)
    {:noreply, state}
  end

  def handle_input({:key, {" ", :press, _}}, _ctx, %{running: false} = state) do
    {:noreply, %{ state | start_time: Time.utc_now, running: true, last_lap_time: Time.utc_now}}
  end

  def handle_input({:key, {" ", :press, _}}, _ctx, %{running: true} = state) do
    {:noreply, %{ state | last_lap_time: Time.utc_now, previous_lap_time: state.last_lap_time }}
  end

  def handle_input({:key, {"escape", :press, _}}, _ctx, %{running: true} = state) do
    {:noreply, %{ state | running: false, previous_lap_time: nil }}
  end

  def handle_input(_input, _context, state), do: {:noreply, state}
end
